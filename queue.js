let collection = [];

// Write the queue functions below.

module.exports = {
    // collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};

function print(item){
    return collection;
}

function enqueue (item) {
    collection[collection.length] = item;
    return collection;
}

function dequeue(item) {
    let dequeuedCollection = []
    for(i=0; i<collection.length-1; i++){
        dequeuedCollection[i] = collection[i+1];
    }
    collection = dequeuedCollection;
    return collection;
}

function front(item){
    let frontitem = collection[0];
    return frontitem;
}

function size(item){
    return collection.length;
}

function isEmpty(item){
    if(collection.length === 0){ return true }
    else return false;
}